package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Region struct {
	Results []struct {
		Name string `json:"name"`
	}
}

func main() {
	resp, err := http.Get("https://pokeapi.co/api/v2/region/")
	if err != nil {
		fmt.Println("The HTTP Request failed with error %s\n", err)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Failed to read into bytes: %s", err)
	}

	var pokemonRegions Region
	err = json.Unmarshal(body, &pokemonRegions)
	if err != nil {
		fmt.Println("Error: ", err)
	}

	fmt.Println(pokemonRegions)

}
